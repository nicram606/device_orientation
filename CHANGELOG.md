## 1.0.0

BREAKING: Raised min SDK to 3.0.0, Flutter 3.16+

* Dependencies upgraded

## 0.0.1

* Current device orientation available with `deviceOrientation` field
* Stream with distinct orientation changes available with `deviceOrientation$` field
* Animated (`AnimatedAlwaysDown`) and non-animated (`AlwaysDown`) widgets matching current absolute device orientation available
