import 'package:device_orientation/widgets/animated_always_down.dart';
import 'package:flutter/material.dart';

/// Rotates given child widget to match
/// device orientation without animation.
/// To use with animation, check [AnimatedAlwaysDown]
/// widget.
class AlwaysDown extends StatelessWidget {
  /// Widget to be rotated.
  final Widget child;

  /// Rotates given [child] according to current device orientation.
  const AlwaysDown({
    super.key,
    required this.child,
  });

  @override
  Widget build(BuildContext context) => AnimatedAlwaysDown(
        duration: Duration.zero,
        child: child,
      );
}
