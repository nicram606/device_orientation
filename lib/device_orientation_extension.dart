part of 'device_orientation.dart';

/// Extension translating device rotation
/// to turns for widget rotation, eg. [RotatedBox], [AnimatedRotation].
extension RotationValue on DeviceOrientation {
  double get turns {
    switch (this) {
      case DeviceOrientation.portraitUp:
        return 0;
      case DeviceOrientation.landscapeLeft:
        return -.25;
      case DeviceOrientation.landscapeRight:
        return .25;
      case DeviceOrientation.portraitDown:
        return .5;
    }
  }
}
