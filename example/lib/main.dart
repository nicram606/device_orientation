import 'package:device_orientation/device_orientation.dart';
import 'package:device_orientation/widgets/always_down.dart';
import 'package:device_orientation/widgets/animated_always_down.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => boot();

Future<void> boot() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Absolute device rotation demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const ExamplePage(),
      );
}

class ExamplePage extends StatelessWidget {
  const ExamplePage({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Down pointers example'),
          systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            StreamBuilder<DeviceOrientation>(
              stream: deviceOrientation$,
              initialData: deviceOrientation,
              builder: (context, snapshot) => Text(snapshot.data!.name),
            ),
            const Center(
              child: Icon(Icons.smartphone, size: 64),
            ),
            const Center(
              child: Icon(Icons.arrow_downward, size: 64),
            ),
            const Center(
              child: AlwaysDown(
                child: Icon(Icons.arrow_downward, size: 64),
              ),
            ),
            const Center(
              child: AnimatedAlwaysDown(
                child: Icon(Icons.arrow_downward, size: 64),
              ),
            ),
          ],
        ),
      );
}
