# Device orientation

Absolute device orientation readings based on accelerometer events.

Use to rotate certain widgets when user rotates the device
without handling complex responsiveness.

![device orientation demo](assets/device_orientation_demo.png)

## Usage

> Note: On devices that does not have or support sensors
> orientation will default to DeviceOrientation.portraitUp.

Listen to a device orientation stream:

```dart
deviceOrientation$.listen((orientation) { 
  print(orientation); 
});
```

Do some work based on current device orientation:

```dart
if (deviceOrientation == DeviceOrientation.portraitUp) {
  // ...
}
```

Use helper widget to make sure that user always sees
critical widgets in correct orientation:

```dart
AnimatedAlwaysDown(
  child: Text("I'm readable!")
)
```

If you does not need animation use:

```dart
AlwaysDown(
  child: Text("I'm readable!")
)
```
